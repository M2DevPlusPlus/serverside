USE [master]
GO
/****** Object:  Database [brixMails]    Script Date: 19/02/2020 14:33:53 ******/
CREATE DATABASE [brixMails]
go
CREATE TABLE [dbo].[FailedLog_Tbl](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[ipAddress] [varchar](100) NULL,
	[failedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WhiteList_Tbl]    Script Date: 19/02/2020 14:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WhiteList_Tbl](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[ipAddress] [varchar](100) NULL,
	[ipDescription] [varchar](100) NULL,
	[isActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FailedLog_Tbl] ON 

INSERT [dbo].[FailedLog_Tbl] ([id], [ipAddress], [failedDate]) VALUES (1, N'192.168.25.154', CAST(N'2020-02-03T11:54:47.157' AS DateTime))
INSERT [dbo].[FailedLog_Tbl] ([id], [ipAddress], [failedDate]) VALUES (2, N'::1', CAST(N'2020-02-03T14:53:55.570' AS DateTime))
INSERT [dbo].[FailedLog_Tbl] ([id], [ipAddress], [failedDate]) VALUES (3, N'::1', CAST(N'2020-02-09T09:51:15.767' AS DateTime))
SET IDENTITY_INSERT [dbo].[FailedLog_Tbl] OFF
SET IDENTITY_INSERT [dbo].[WhiteList_Tbl] ON 

INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (1, N'192.168.22.51', N'office212', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (2, N'192.168.0.172', N'office3121', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (3, N'192.168.25.145', N'brix123', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (4, N'192.216.255.123', N'ssss5', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (5, N'123.45.123.0', N'cvbnm11111', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (6, N'123.132.132.165', N'!!!!!!!!!!!!!!', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (7, N'123.125.125.145', N'דעגעי', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (8, N'142.254.124.211', N'xcvbnm', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (9, N'145.125.147.159', N'xcvb', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (10, N'133.25.46.14', N'cvbnm,', 0)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (11, N'123.250.124.146', N'1234', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (12, N'::1', N'localhost', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (13, N'127.0.0.1', N'מקומי2', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (14, N'192.168.25.146', N'CrossRiver1', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (15, N'192.158.125.146', N'xsdfvb', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (16, N'192.168.14.125', N'cms', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (21, N'192.254.14.156', N'ghj', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (22, N'125.145.15.148', N'1234', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (23, N'123.145.154.123', N'xcnhk', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (24, N'125.147.125.149', N'cvcv', 1)
INSERT [dbo].[WhiteList_Tbl] ([id], [ipAddress], [ipDescription], [isActive]) VALUES (25, N'250.23.154.124', N'ccb', 1)
SET IDENTITY_INSERT [dbo].[WhiteList_Tbl] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__WhiteLis__89E5C7F0FD30C784]    Script Date: 19/02/2020 14:33:53 ******/
ALTER TABLE [dbo].[WhiteList_Tbl] ADD UNIQUE NONCLUSTERED 
(
	[ipAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[AddFailed]    Script Date: 19/02/2020 14:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddFailed]
(
@ipAddress varchar(100),
@failedDate datetime
)
as
begin
insert into [dbo].[FailedLog_Tbl] VALUES(@ipAddress,@failedDate)
end
GO
/****** Object:  StoredProcedure [dbo].[AddIP]    Script Date: 19/02/2020 14:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[AddIP]
(
@ipAddress varchar(100),
@ipDescription varchar(100)
)
as
begin
insert into [dbo].[WhiteList_Tbl] VALUES(@ipAddress,@ipDescription,1)
end
GO
/****** Object:  StoredProcedure [dbo].[DeleteIp]    Script Date: 19/02/2020 14:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DeleteIp]
(@id varchar(20))
as
begin
update [dbo].WhiteList_Tbl
set [isActive]=0 where id=@id
end
GO
/****** Object:  StoredProcedure [dbo].[getAllIp]    Script Date: 19/02/2020 14:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getAllIp]
as
begin
select id,ipAddress,ipDescription from [dbo].[WhiteList_Tbl]
where isActive = 1
end
GO
/****** Object:  StoredProcedure [dbo].[isIpExsict]    Script Date: 19/02/2020 14:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[isIpExsict]
(
@ipAddress varchar(20)
)
as
begin
select id,ipAddress,ipDescription from [dbo].[WhiteList_Tbl] where [ipAddress] = @ipAddress
end
GO
/****** Object:  StoredProcedure [dbo].[UpdateIP]    Script Date: 19/02/2020 14:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateIP]
(
@id smallint,
@ipAddress varchar(100),
@ipDescription varchar(100)
)
as
UPDATE  [dbo].[WhiteList_Tbl] 
SET ipAddress = @ipAddress,
 ipDescription=@ipDescription
WHERE  id=@id
GO
USE [master]
GO
ALTER DATABASE [brixMails] SET  READ_WRITE 
GO
