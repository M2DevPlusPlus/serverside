﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace CheckSendMail
{
    public class MailMessageDTO
    {
        public string To { get; set; }
        public string CC { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachments { get; set; }

    }
}
