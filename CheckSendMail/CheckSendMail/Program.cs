﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using System.Linq;

namespace CheckSendMail
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static async Task Main()
        {
            MailMessage.SendRequest mail = new MailMessage.SendRequest();
              Console.WriteLine("Enter comma-separated recipient addresses list ");
             mail.listTo=(Console.ReadLine()).Split(',').ToList();
            //mail.listTo = new System.Collections.Generic.List<string>() { "tf7617858@gmail.com" };
            //Console.WriteLine("Enter comma-separated cc addresses list ");
            Console.WriteLine("enter subject");
            mail.subject = Console.ReadLine();

            Console.WriteLine("Enter Mail content ");

            mail.body = Console.ReadLine();
            try

            {
                var myContent = JsonConvert.SerializeObject(mail);
                var buffer = Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/json");
                var result = client.PostAsync("https://localhost:44300/api/Mail/SendEmail", byteContent).Result;
                result.EnsureSuccessStatusCode();
                string responseBody = await result.Content.ReadAsStringAsync();
                Console.WriteLine(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }


        }
    }
}

