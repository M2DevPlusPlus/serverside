﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class WhiteListDTO
    {
        [Identity]
        public int id { get; set; }
        public string ipAddress { get; set; }
        public string ipDescription { get; set; }


    }
}