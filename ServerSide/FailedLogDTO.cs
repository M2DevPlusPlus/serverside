﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
 public class FailedLogDTO
    {
        [Identity]
        public int id { get; set; }
        public string ipAddress { get; set; }
        public DateTime failedDate { get; set; }
    }
}